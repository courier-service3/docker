# Сборка всего проекта
build:
	docker-compose build

# Поднятие всего проекта
up:
	docker-compose up -d

# Просмотр активных сервисов
ps:
	docker-compose ps

# Положить все сервисы
down:
	docker-compose down

# Рестартануть все сервисы
restart:
	docker-compose restart

# Пересобрать весь проект
rebuild:
	docker-compose down
	docker-compose build
	docker-compose up -d
	sh install.sh

# Поднять только сервис nginx
up-nginx:
	docker-compose up -d nginx --build

# Положить только сервис nginx
down-nginx:
	docker-compose down nginx

# Поднять только сервис courier-call
up-api:
	docker-compose up -d courier-call-php-fpm --build

# Положить только сервис courier-call
down-api:
	docker-compose down courier-call-php-fpm

# Консоль для courier-call
api-php:
	docker-compose exec courier-call-api-php-fpm bash
