#!/bin/bash
docker-compose exec -T courier-call-api-php-fpm chmod -R 777 storage/ bootstrap/
docker-compose exec -T courier-call-api-php-fpm cp .env.example .env
docker-compose exec -T courier-call-api-php-fpm composer install
docker-compose exec -T courier-call-api-php-fpm php artisan l5-swagger:generate
docker-compose exec -T courier-call-api-php-fpm php artisan key:generate
docker-compose exec -T courier-call-api-php-fpm php artisan migrate
docker-compose exec -T courier-call-api-php-fpm php artisan db:seed
docker-compose exec -T courier-call-api-php-fpm php artisan jwt:secret

